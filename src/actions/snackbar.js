export const SHOW_SNACKBAR = "SHOW_SNACKBAR"
export const HIDE_SNACKBAR = "HIDE_SNACKBAR"

export const showSnackbar = payload => ({
  type: SHOW_SNACKBAR,
  payload,
})

export const hideSnackbar = () => ({
  type: HIDE_SNACKBAR,
})
