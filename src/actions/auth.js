export const LOGIN_REQUESTED = "LOGIN_REQUESTED"
export const LOGIN_RECEIVED = "LOGIN_RECEIVED"
export const LOGIN_FAILED = "LOGIN_FAILED"
export const SIGNUP_REQUESTED = "SIGNUP_REQUESTED"
export const SIGNUP_RECEIVED = "SIGNUP_RECEIVED"
export const SIGNUP_FAILED = "SIGNUP_FAILED"
export const LOGOUT = "LOGOUT"
export const LOGOUT_RECEIVED = "LOGOUT_RECEIVED"

export const requestLogin = payload => ({
  type: LOGIN_REQUESTED,
  payload,
})

export const loginReceived = () => ({
  type: LOGIN_RECEIVED,
})

export const loginFailed = payload => ({
  type: LOGIN_FAILED,
  payload,
})

export const requestSignup = payload => ({
  type: SIGNUP_REQUESTED,
  payload,
})

export const signupReceived = payload => ({
  type: SIGNUP_RECEIVED,
  payload,
})

export const signupFailed = payload => ({
  type: SIGNUP_FAILED,
  payload,
})

export const logout = () => ({ type: LOGOUT })
export const logoutReceived = () => ({ type: LOGOUT_RECEIVED })
