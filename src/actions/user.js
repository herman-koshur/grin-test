export const REQUEST_USER_DATA = "REQUEST_USER_DATA"
export const REQUEST_USER_DATA_DETAILS = "REQUEST_USER_DATA_DETAILS"
export const REQUEST_USER_DATA_IMAGE = "REQUEST_USER_DATA_IMAGE"
export const USER_DATA_RECEIVED = "USER_DATA_RECEIVED"
export const REQUEST_USER_DATA_FAILED = "REQUEST_USER_DATA_FAILED"

export const UPDATE_USER_DATA = "UPDATE_USER_DATA"
export const UPDATE_USER_DATA_RECEIVED = "UPDATE_USER_DATA_RECEIVED"
export const UPDATE_USER_DATA_FAILED = "UPDATE_USER_DATA_FAILED"

export const UPDATE_USER_IMAGE = "UPDATE_USER_IMAGE"
export const UPDATE_USER_IMAGE_RECEIVED = "UPDATE_USER_IMAGE_RECEIVED"
export const UPDATE_USER_IMAGE_FAILED = "UPDATE_USER_IMAGE_FAILED"

export const requestUserData = () => ({ type: REQUEST_USER_DATA })
export const requestUserDataDetails = () => ({
  type: REQUEST_USER_DATA_DETAILS,
})
export const requestUserDataImage = () => ({ type: REQUEST_USER_DATA_IMAGE })

export const userDataReceived = payload => ({
  type: USER_DATA_RECEIVED,
  payload,
})

export const requestUserDataFailed = payload => ({
  type: REQUEST_USER_DATA_FAILED,
  payload,
})

export const updateUserData = payload => ({
  type: UPDATE_USER_DATA,
  payload,
})

export const updateUserDataReceived = payload => ({
  type: UPDATE_USER_DATA_RECEIVED,
  payload,
})

export const updateUserDataFailed = payload => ({
  type: UPDATE_USER_DATA_FAILED,
  payload,
})

export const updateUserImage = payload => ({
  type: UPDATE_USER_IMAGE,
  payload,
})

export const updateUserImageReceived = payload => ({
  type: UPDATE_USER_IMAGE_RECEIVED,
  payload,
})

export const updateUserImageFailed = payload => ({
  type: UPDATE_USER_IMAGE_FAILED,
  payload,
})
