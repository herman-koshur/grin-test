import { ofType } from "redux-observable"
import { of } from "rxjs"
import { switchMap, mergeMap, catchError, tap, mapTo } from "rxjs/operators"
import { push } from "connected-react-router"
import { setToken, removeToken, errorHandler } from "../utils"
import * as Actions from "../actions"
import * as api from "../api"
import { routes } from "../constants"

export const requestLoginEpic = action$ =>
  action$.pipe(
    ofType(Actions.LOGIN_REQUESTED),
    switchMap(({ payload }) =>
      api.loginRequest(payload).pipe(
        mergeMap(result => setToken(result, Actions.loginReceived)),
        catchError(({ response }) => of(Actions.loginFailed(response))),
      ),
    ),
  )

export const loginFailedEpic = action$ =>
  action$.pipe(
    ofType(Actions.LOGIN_FAILED),
    switchMap(({ payload }) =>
      of(Actions.showSnackbar({ type: "error", text: payload.general })),
    ),
  )

export const requestSignupEpic = action$ =>
  action$.pipe(
    ofType(Actions.SIGNUP_REQUESTED),
    switchMap(({ payload }) =>
      api.signupRequest(payload).pipe(
        mergeMap(result => setToken(result, Actions.signupReceived)),
        catchError(({ response }) => of(Actions.signupFailed(response))),
      ),
    ),
  )

export const signupFailedEpic = action$ =>
  action$.pipe(
    ofType(Actions.SIGNUP_FAILED),
    switchMap(({ payload }) => {
      const message = errorHandler(payload)
      return of(Actions.showSnackbar({ type: "error", text: message }))
    }),
  )

export const logoutEpic = action$ =>
  action$.pipe(
    ofType(Actions.LOGOUT),
    tap(removeToken),
    mapTo(Actions.logoutReceived()),
  )

export const logoutReceivedEpic = action$ =>
  action$.pipe(ofType(Actions.LOGOUT_RECEIVED), mapTo(push(routes.LOGIN)))
