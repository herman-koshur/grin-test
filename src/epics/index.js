import { combineEpics } from "redux-observable"
import * as authEpics from "./auth"
import * as userEpics from "./user"

export default combineEpics(
  ...Object.values({
    ...authEpics,
    ...userEpics,
  }),
)
