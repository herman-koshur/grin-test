import { ofType } from "redux-observable"
import { push } from "connected-react-router"
import { of, from } from "rxjs"
import {
  mapTo,
  switchMap,
  switchMapTo,
  mergeMap,
  catchError,
} from "rxjs/operators"
import * as Actions from "../actions"
import * as api from "../api"
import { routes } from "../constants"
import { privateEndpointsErrorHandler } from "../utils"

export const redirectToUserPageEpic = action$ =>
  action$.pipe(
    ofType(Actions.LOGIN_RECEIVED, Actions.SIGNUP_RECEIVED),
    mapTo(push(routes.USER_PAGE)),
  )

export const triggerRequestUserDataDetailsEpic = action$ =>
  action$.pipe(
    ofType(Actions.UPDATE_USER_DATA_RECEIVED),
    mapTo(Actions.requestUserDataDetails()),
  )

export const triggerRequestUserDataImageEpic = action$ =>
  action$.pipe(
    ofType(Actions.UPDATE_USER_IMAGE_RECEIVED),
    mapTo(Actions.requestUserDataImage()),
  )

export const requestUserDataEpic = action$ =>
  action$.pipe(
    ofType(
      Actions.REQUEST_USER_DATA,
      Actions.REQUEST_USER_DATA_DETAILS,
      Actions.REQUEST_USER_DATA_IMAGE,
    ),
    switchMapTo(
      from(api.userDataRequest()).pipe(
        mergeMap(({ response }) => of(Actions.userDataReceived(response))),
        catchError(({ status }) =>
          privateEndpointsErrorHandler(status, Actions.requestUserDataFailed),
        ),
      ),
    ),
  )

export const requestUserDataFailedEpic = action$ =>
  action$.pipe(
    ofType(Actions.REQUEST_USER_DATA_FAILED),
    mapTo(
      Actions.showSnackbar({
        type: "error",
        text: "Get user data attempt was failed",
      }),
    ),
  )

export const updateUserDataEpic = action$ =>
  action$.pipe(
    ofType(Actions.UPDATE_USER_DATA),
    switchMap(({ payload }) =>
      api.updateUserData(payload).pipe(
        mergeMap(({ response }) =>
          of(Actions.updateUserDataReceived(response.message)),
        ),
        catchError(({ status }) =>
          privateEndpointsErrorHandler(status, Actions.updateUserDataFailed),
        ),
      ),
    ),
  )

export const updateUserDataReceivedEpic = action$ =>
  action$.pipe(
    ofType(Actions.UPDATE_USER_DATA_RECEIVED),
    switchMap(({ payload }) =>
      of(Actions.showSnackbar({ type: "success", text: payload })),
    ),
  )

export const updateUserDataFailedEpic = action$ =>
  action$.pipe(
    ofType(Actions.UPDATE_USER_DATA_FAILED),
    mapTo(
      Actions.showSnackbar({
        type: "error",
        text: "Update user data attempt was failed",
      }),
    ),
  )

export const updateUserImageEpic = action$ =>
  action$.pipe(
    ofType(Actions.UPDATE_USER_IMAGE),
    switchMap(({ payload }) =>
      api.updateUserImage(payload).pipe(
        mergeMap(({ response }) =>
          of(Actions.updateUserImageReceived(response.message)),
        ),
        catchError(({ status }) =>
          privateEndpointsErrorHandler(status, Actions.updateUserImageFailed),
        ),
      ),
    ),
  )

export const updateUserImageReceivedEpic = action$ =>
  action$.pipe(
    ofType(Actions.UPDATE_USER_IMAGE_RECEIVED),
    switchMap(({ payload }) =>
      of(Actions.showSnackbar({ type: "success", text: payload })),
    ),
  )

export const updateUserImageFailedEpic = action$ =>
  action$.pipe(
    ofType(Actions.UPDATE_USER_IMAGE_FAILED),
    mapTo(
      Actions.showSnackbar({
        type: "error",
        text: "Update user image attempt was failed",
      }),
    ),
  )
