import { ajax } from "rxjs/ajax"
import { endpoints } from "../constants"

export const loginRequest = payload => {
  return ajax({
    url: endpoints.login,
    method: "POST",
    body: payload,
    crossDomain: true,
  })
}

export const signupRequest = payload => {
  return ajax({
    url: endpoints.registration,
    method: "POST",
    body: payload,
    crossDomain: true,
  })
}
