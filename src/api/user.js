import { ajax } from "rxjs/ajax"
import { endpoints } from "../constants"
import { defaultHeaders } from "../utils"

export const userDataRequest = () => {
  return ajax({
    url: endpoints.user,
    method: "GET",
    headers: defaultHeaders(),
    crossDomain: true,
  })
}

export const updateUserData = payload => {
  return ajax({
    url: endpoints.user,
    method: "POST",
    headers: defaultHeaders(),
    body: payload,
    crossDomain: true,
  })
}

export const updateUserImage = payload => {
  return ajax({
    url: endpoints.uploadImage,
    method: "POST",
    headers: defaultHeaders(),
    body: payload,
    crossDomain: true,
  })
}
