const API_ENV = process.env.REACT_APP_API

const API = `${API_ENV}/api`

export const endpoints = {
  login: `${API}/login`,
  registration: `${API}/signup`,
  user: `${API}/user`,
  uploadImage: `${API}/user/image`,
}

export const routes = {
  LOGIN: "/login",
  REGISTRATION: "/signup",
  USER_PAGE: "/",
}
