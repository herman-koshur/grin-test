import {
  LocalStorageMock,
  defaultHeaders,
  getToken,
  removeToken,
  errorHandler,
} from "."

test("Default headers function", () => {
  window.localStorage = new LocalStorageMock()
  localStorage.setItem("token", "mocked_token")
  expect(defaultHeaders()).toEqual({
    "Access-Control-Allow-Origin": "*",
    Authorization: "Bearer mocked_token",
  })
})

test("Get token function", () => {
  window.localStorage = new LocalStorageMock()
  localStorage.setItem("token", "mocked_token")
  expect(getToken()).toBe("mocked_token")
})

test("Remove token function", () => {
  window.localStorage = new LocalStorageMock()
  localStorage.setItem("token", "mocked_token")
  removeToken()
  expect(localStorage.getItem("token")).toBe(null)
})

test("Error handler function", () => {
  expect(errorHandler()).toEqual("Something went wrong!")
  expect(errorHandler({ email: "Wrong credentials" })).toEqual(
    "Wrong credentials",
  )
  expect(errorHandler({ handle: "This nickname is already taken" })).toEqual(
    "This nickname is already taken",
  )
  expect(errorHandler({ general: "Internal Server Error" })).toEqual(
    "Internal Server Error",
  )
})
