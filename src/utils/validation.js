const patterns = {
  email: /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/,
  password: /^(?=.*[A-Za-z])(?=.*d)[A-Za-zd]{8,}$/,
  website:
    /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/,
}

export const validationMessages = {
  email: {
    required: "The email field is required",
    pattern: "Please enter a valid email address",
  },
  password: {
    required: "The password field is required",
    pattern:
      "Password should contain minimum eight characters, at least one letter and one number",
  },
  confirmPassword: { validate: "The passwords do not match" },
  nickname: {
    required: "The nickname field is required",
    minLength: "The nickhame should contain minimum three characters",
  },
  bio: { maxLength: "The bio should contain maximum 30 characters" },
  website: {
    maxLength: "The website should contain maximum 30 characters",
    pattern: "Please enter a valid url",
  },
  location: { maxLength: "The location should contain maximum 20 characters" },
}

export const validationRules = {
  email: {
    required: validationMessages.email.required,
    pattern: {
      value: patterns.email,
      message: validationMessages.email.pattern,
    },
  },
  password: {
    required: validationMessages.password.required,
  },
  signupPassword: {
    required: validationMessages.password.required,
    pattern: {
      value: patterns.password,
      message: validationMessages.password.pattern,
    },
  },
  nickname: {
    required: validationMessages.nickname.required,
    minLength: {
      value: 3,
      message: validationMessages.nickname.minLength,
    },
  },
  bio: {
    maxLength: {
      value: 30,
      message: validationMessages.bio.maxLength,
    },
  },
  location: {
    maxLength: {
      value: 20,
      message: validationMessages.location.maxLength,
    },
  },
  website: {
    pattern: {
      value: patterns.website,
      message: validationMessages.website.pattern,
    },
    maxLength: {
      value: 30,
      message: validationMessages.website.maxLength,
    },
  },
}
