import React, { useState, useEffect } from "react"
import { useDispatch } from "react-redux"
import { Route, Redirect } from "react-router"
import { getToken } from "."

import { showSnackbar } from "../actions"

export const PrivateRoute = ({ component: Component, ...rest }) => {
  const [isAuthenticated, setIsAuthenticated] = useState(!!getToken())
  const dispatch = useDispatch()

  const recheckToken = () => {
    const isTokenExist = !!getToken()
    if (!isTokenExist)
      dispatch(
        showSnackbar({
          type: "error",
          text: "Token was lost. Please sign in again!",
        }),
      )
    setIsAuthenticated(isTokenExist)
  }

  useEffect(() => {
    window.addEventListener("storage", recheckToken)

    return () => {
      window.removeEventListener("storage", recheckToken)
    }
  })

  return (
    <Route
      {...rest}
      render={props =>
        isAuthenticated ? <Component {...props} /> : <Redirect to="/login" />
      }
    />
  )
}
