import { of, concat } from "rxjs"
import { logout, showSnackbar } from "../actions"

export const defaultHeaders = () => {
  const token = localStorage.getItem("token")
  return {
    Authorization: `Bearer ${token}`,
    "Access-Control-Allow-Origin": "*",
  }
}

export const getToken = () => {
  return localStorage.getItem("token")
}

export const setToken = (result, action) => {
  const { token } = result.response
  localStorage.setItem("token", token)
  return of(action())
}

export const removeToken = () => {
  localStorage.removeItem("token")
}

export const errorHandler = (payload = {}) => {
  const { email, handle, general } = payload
  return email || handle || general || "Something went wrong!"
}

export const privateEndpointsErrorHandler = (status, action) => {
  if (status === 403) {
    return concat(
      of(action()),
      of(
        showSnackbar({
          type: "error",
          text: "You were logged out cause access was forbidden by system",
          duration: 5000,
        }),
      ),
      of(logout()),
    )
  }
  return of(action())
}
