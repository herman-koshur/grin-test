import React from "react"
import { Switch, Route } from "react-router"
import { ConnectedRouter } from "connected-react-router"
import { Provider } from "react-redux"
import { PrivateRoute, history } from "./utils"
import { routes } from "./constants/api"
import configureStore from "./store"

import { AppLayout } from "./components/layouts"
import UserPage from "./components/Profile"
import Login from "./components/Login"
import Registration from "./components/Registration"
import { Snackbar } from "./components/common"

const store = configureStore()

function App() {
  return (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <AppLayout>
          <Switch>
            <PrivateRoute exact path={routes.USER_PAGE} component={UserPage} />
            <Route exact path={routes.LOGIN} component={Login} />
            <Route exact path={routes.REGISTRATION} component={Registration} />
          </Switch>
          <Snackbar />
        </AppLayout>
      </ConnectedRouter>
    </Provider>
  )
}

export default App
