import * as Actions from "../actions"

const initialState = {
  open: false,
  duration: 3000,
  text: "",
  type: "info",
}

const SnackbarReducer = (state = initialState, action) => {
  switch (action.type) {
    case Actions.SHOW_SNACKBAR:
      return {
        ...state,
        open: true,
        text: action.payload.text,
        type: action.payload.type || "info",
        duration: action.payload.duration || 3000,
      }

    case Actions.HIDE_SNACKBAR:
      return {
        ...state,
        open: false,
        text: "",
      }

    default:
      return state
  }
}

export default SnackbarReducer
