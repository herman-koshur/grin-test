import { combineReducers } from "redux"
import { connectRouter } from "connected-react-router"
import authReducer from "./auth"
import userReducer from "./user"
import snackbarReducer from "./snackbar"

const rootReducer = history =>
  combineReducers({
    auth: authReducer,
    user: userReducer,
    router: connectRouter(history),
    snackbar: snackbarReducer,
  })

export default rootReducer
