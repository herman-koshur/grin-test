import * as Actions from "../actions"

const initialState = {
  loading: false,
  success: false,
  error: "",
}

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case Actions.LOGIN_REQUESTED:
    case Actions.SIGNUP_REQUESTED:
    case Actions.LOGOUT:
      return {
        ...state,
        loading: true,
        success: false,
        error: "",
      }
    case Actions.LOGIN_RECEIVED:
    case Actions.LOGOUT_RECEIVED:
      return {
        ...state,
        loading: false,
        success: true,
        error: "",
      }
    case Actions.LOGIN_FAILED:
    case Actions.SIGNUP_FAILED:
      return {
        ...state,
        success: false,
        error: action.payload,
        loading: false,
      }
    default:
      return state
  }
}

export default authReducer
