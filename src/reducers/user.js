import * as Actions from "../actions"

const initialState = {
  loading: false,
  imageLoading: false,
  success: false,
  error: "",
  data: {},
}

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case Actions.REQUEST_USER_DATA:
      return {
        ...state,
        loading: true,
        imageLoading: true,
        success: false,
        error: "",
      }
    case Actions.UPDATE_USER_DATA:
    case Actions.REQUEST_USER_DATA_DETAILS:
      return {
        ...state,
        loading: true,
        success: false,
        error: "",
      }
    case Actions.UPDATE_USER_IMAGE:
    case Actions.REQUEST_USER_DATA_IMAGE:
      return {
        ...state,
        imageLoading: true,
        success: false,
        error: "",
      }
    case Actions.UPDATE_USER_DATA_RECEIVED:
      return {
        ...state,
        success: true,
        error: "",
        loading: false,
      }
    case Actions.UPDATE_USER_IMAGE_RECEIVED:
      return {
        ...state,
        success: true,
        error: "",
        imageLoading: false,
      }
    case Actions.USER_DATA_RECEIVED:
      return {
        ...state,
        loading: false,
        imageLoading: false,
        success: true,
        error: "",
        data: {
          ...action.payload.credentials,
        },
      }
    case Actions.REQUEST_USER_DATA_FAILED:
      return {
        ...state,
        success: false,
        error: action.payload,
        loading: false,
        imageLoading: false,
      }
    default:
      return state
  }
}

export default userReducer
