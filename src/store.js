import { createStore, applyMiddleware, compose } from "redux"
import { createEpicMiddleware } from "redux-observable"
import { routerMiddleware as createRouterMiddleware } from "connected-react-router"
import { composeWithDevTools } from "redux-devtools-extension"
import rootEpic from "./epics"
import rootReducer from "./reducers"
import { history } from "./utils"

const epicMiddleware = createEpicMiddleware({
  dependencies: { history },
})
const routerMiddleware = createRouterMiddleware(history)
const composeEnhancers =
  process.env.NODE_ENV !== "production" ? composeWithDevTools({}) : compose

export default function configureStore() {
  const store = createStore(
    rootReducer(history),
    composeEnhancers(applyMiddleware(epicMiddleware, routerMiddleware)),
  )

  epicMiddleware.run(rootEpic)

  return store
}
