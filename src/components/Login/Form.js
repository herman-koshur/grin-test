import React from "react"
import { useDispatch, useSelector } from "react-redux"
import { useForm } from "react-hook-form"

import TextField from "@mui/material/TextField"
import { Form, Button } from "../common"

import { requestLogin } from "../../actions"

import { validationRules } from "../../utils"

export const LoginForm = () => {
  const dispatch = useDispatch()
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm()

  const { loading } = useSelector(state => state.auth)

  const loginAttempt = data => {
    dispatch(requestLogin(data))
  }

  return (
    <Form onSubmit={handleSubmit(loginAttempt)}>
      <TextField
        label="Email"
        variant="standard"
        error={!!errors.email}
        helperText={errors.email?.message}
        {...register("email", validationRules.email)}
      />
      <TextField
        type="password"
        label="Password"
        variant="standard"
        error={!!errors.password}
        helperText={errors.password?.message}
        {...register("password", validationRules.password)}
      />
      <Button type="submit" disabled={loading} loading={loading}>
        Login
      </Button>
    </Form>
  )
}
