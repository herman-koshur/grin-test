import React from "react"
import { routes } from "../../constants/api"

import { AuthLayout } from "../layouts"
import { LoginForm } from "./Form"

const Login = () => {
  return (
    <AuthLayout
      header="Login"
      text="Don't have an account? Sign up"
      link={routes.REGISTRATION}
    >
      <LoginForm />
    </AuthLayout>
  )
}

export default Login
