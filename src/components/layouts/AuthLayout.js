import React from "react"
import styled from "@emotion/styled"
import PropTypes from "prop-types"
import { Link } from "react-router-dom"

import MaterialTypography from "@mui/material/Typography"

const Root = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  max-width: 700px;
  width: 100%;
  padding: 0 20px;
`

const Typography = styled(MaterialTypography)`
  margin: 20px 0;
`

export const AuthLayout = ({ header, text, link, children }) => {
  return (
    <Root>
      <Typography variant="h2">{header}</Typography>
      {children}
      <Typography variant="body2">
        {text} <Link to={link}>here</Link>
      </Typography>
    </Root>
  )
}

Typography.propTypes = {
  header: PropTypes.string,
  text: PropTypes.string,
  link: PropTypes.string,
  children: PropTypes.node.isRequired,
}
