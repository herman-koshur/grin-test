import React, { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"

import Button from "@mui/material/Button"
import { Paper, ButtonGroup } from "../common"
import { EditProfileModal } from "./EditProfile"
import { ProfileAvatar } from "./ProfileAvatar"
import { ProfileInfo } from "./ProfileInfo"

import { requestUserData, logout } from "../../actions"
import { getUserData } from "../../selectors"

const Profile = () => {
  const [showEditProfileModal, setShowEditProfileModal] = useState(false)
  const dispatch = useDispatch()

  const { bio, location, website } = useSelector(state => getUserData(state))

  useEffect(() => {
    dispatch(requestUserData())
  }, [dispatch])

  const handleShowEditProfileModal = () => {
    setShowEditProfileModal(true)
  }

  const handleCloseEditProfileModal = () => {
    setShowEditProfileModal(false)
  }

  const handleLogout = () => {
    dispatch(logout())
  }

  useEffect(() => {
    handleCloseEditProfileModal()
  }, [bio, location, website])

  return (
    <Paper>
      <ProfileAvatar />
      <ProfileInfo onOpenModal={handleShowEditProfileModal} />
      <ButtonGroup>
        <Button variant="contained" onClick={handleShowEditProfileModal}>
          Edit profile
        </Button>
        <Button variant="contained" color="error" onClick={handleLogout}>
          Logout
        </Button>
      </ButtonGroup>
      <EditProfileModal
        open={showEditProfileModal}
        onClose={handleCloseEditProfileModal}
      />
    </Paper>
  )
}

export default Profile
