import React from "react"
import { useSelector } from "react-redux"
import PropTypes from "prop-types"

import { Typography, Link } from "../common"

import { getUserData } from "../../selectors"

export const ProfileInfo = ({ onOpenModal }) => {
  const { createdAt, bio, nickname, location, website, email } = useSelector(
    state => getUserData(state),
  )
  const { loading } = useSelector(state => state.user)

  return (
    <>
      <Typography variant="h6" loading={loading}>
        @{nickname}
      </Typography>
      <Typography
        variant="body1"
        loading={loading}
        loadingWidth={150}
        name="bio"
        value={bio}
        onClick={onOpenModal}
      >
        {bio}
      </Typography>
      <Typography
        variant="body1"
        loading={loading}
        loadingWidth={120}
        name="location"
        value={location}
        onClick={onOpenModal}
      >
        {location}
      </Typography>
      <Typography
        variant="body1"
        color="primary"
        loading={loading}
        loadingWidth={160}
      >
        <Link href={`mailto:${email}`}>{email}</Link>
      </Typography>
      <Typography
        variant="body1"
        color="primary"
        loading={loading}
        loadingWidth={130}
        name="website"
        value={website}
        onClick={onOpenModal}
      >
        <Link href={website} target="_blank" rel="noopener noreferrer">
          {website}
        </Link>
      </Typography>
      <Typography variant="body1" loading={loading} loadingWidth={120}>
        {createdAt}
      </Typography>
    </>
  )
}

ProfileInfo.propTypes = {
  onOpenModal: PropTypes.func.isRequired,
}
