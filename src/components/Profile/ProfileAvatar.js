import React, { useRef } from "react"
import styled from "@emotion/styled"
import { useSelector, useDispatch } from "react-redux"

import AddAPhotoIcon from "@mui/icons-material/AddAPhoto"
import { Avatar, IconButton } from "../common"

import { updateUserImage } from "../../actions"
import { getUserData } from "../../selectors"

const AvatarWrapper = styled.div`
  position: relative;
`

const IconWrapper = styled.div`
  position: absolute;
  bottom: 0;
  right: 0;
`

export const ProfileAvatar = () => {
  const dispatch = useDispatch()
  const { imageUrl } = useSelector(state => getUserData(state))
  const { imageLoading } = useSelector(state => state.user)
  const imageInput = useRef(null)

  const handleChangeAvatar = () => {
    imageInput.current.click()
  }

  const handleInputChange = event => {
    const image = event.target.files[0]
    if (image) {
      const formData = new FormData()
      formData.append("image", image, image.name)
      dispatch(updateUserImage(formData))
    }
  }

  return (
    <AvatarWrapper>
      <Avatar
        src={imageUrl}
        size={{ width: 150, height: 150 }}
        alt="user avatar"
        loading={imageLoading}
        onClick={handleChangeAvatar}
      />
      <input
        type="file"
        accept="image/jpeg,image/png"
        hidden
        ref={imageInput}
        onChange={handleInputChange}
      />
      <IconWrapper>
        <IconButton tooltip="Upload image" onClick={handleChangeAvatar}>
          <AddAPhotoIcon />
        </IconButton>
      </IconWrapper>
    </AvatarWrapper>
  )
}
