import React from "react"
import { useDispatch, useSelector } from "react-redux"
import { useForm } from "react-hook-form"
import PropTypes from "prop-types"

import TextField from "@mui/material/TextField"
import Dialog from "@mui/material/Dialog"
import { Form, ButtonGroup, Button } from "../common"

import { updateUserData, showSnackbar } from "../../actions"
import { getUserData } from "../../selectors"

import { validationRules } from "../../utils"

export const EditProfileModal = ({ open, onClose }) => {
  const dispatch = useDispatch()
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ shouldUnregister: true })

  const { bio, location, website } = useSelector(state => getUserData(state))
  const { loading } = useSelector(state => state.user)

  const handleApplyChanges = data => {
    if (
      bio === data.bio &&
      location === data.location &&
      website === data.website
    ) {
      onClose()
      dispatch(
        showSnackbar({
          text: "Details remained as before cause you didn't change anything",
          duration: 6000,
        }),
      )
    } else {
      dispatch(updateUserData(data))
    }
  }

  return (
    <Dialog
      open={open}
      onClose={onClose}
      maxWidth="sm"
      fullWidth
      aria-labelledby="edit profile modal"
    >
      <Form
        onSubmit={handleSubmit(handleApplyChanges)}
        maxWidth="none"
        margin="20px"
      >
        <TextField
          defaultValue={bio}
          label="Bio"
          variant="standard"
          error={!!errors.bio}
          helperText={errors.bio?.message}
          {...register("bio", validationRules.bio)}
        />
        <TextField
          defaultValue={website}
          label="Website"
          variant="standard"
          error={!!errors.website}
          helperText={errors.website?.message}
          {...register("website", validationRules.website)}
        />
        <TextField
          defaultValue={location}
          label="Location"
          variant="standard"
          error={!!errors.location}
          helperText={errors.location?.message}
          {...register("location", validationRules.location)}
        />
        <ButtonGroup>
          <Button type="submit" disabled={loading} loading={loading}>
            Apply changes
          </Button>
          <Button color="error" onClick={onClose}>
            Cancel
          </Button>
        </ButtonGroup>
      </Form>
    </Dialog>
  )
}

EditProfileModal.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
}
