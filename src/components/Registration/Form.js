import React from "react"
import { useDispatch, useSelector } from "react-redux"
import { useForm } from "react-hook-form"

import TextField from "@mui/material/TextField"
import { Form, Button } from "../common"

import { requestSignup } from "../../actions"

import { validationRules, validationMessages } from "../../utils"

export const RegistrationForm = () => {
  const dispatch = useDispatch()
  const {
    register,
    watch,
    handleSubmit,
    formState: { errors },
  } = useForm()

  const { loading } = useSelector(state => state.auth)

  const password = watch("password")

  const signupAttempt = data => {
    dispatch(requestSignup(data))
  }

  return (
    <Form onSubmit={handleSubmit(signupAttempt)}>
      <TextField
        label="Email"
        variant="standard"
        error={!!errors.email}
        helperText={errors.email?.message}
        {...register("email", validationRules.email)}
      />
      <TextField
        type="password"
        label="Password"
        variant="standard"
        error={!!errors.password}
        helperText={errors.password?.message}
        {...register("password", validationRules.password)}
      />
      <TextField
        type="password"
        label="Confirm Password"
        variant="standard"
        error={!!errors.confirmPassword}
        helperText={errors.confirmPassword?.message}
        {...register("confirmPassword", {
          validate: value =>
            value === password || validationMessages.confirmPassword.validate,
        })}
      />
      <TextField
        label="Nickname"
        variant="standard"
        error={!!errors.handle}
        helperText={errors.handle?.message}
        {...register("handle", validationRules.nickname)}
      />
      <Button type="submit" disabled={loading} loading={loading}>
        Sign up
      </Button>
    </Form>
  )
}
