import React from "react"
import { routes } from "../../constants/api"

import { AuthLayout } from "../layouts"
import { RegistrationForm } from "./Form"

const Registration = () => {
  return (
    <AuthLayout
      header="Registration"
      text="Already have an account? Login"
      link={routes.LOGIN}
    >
      <RegistrationForm />
    </AuthLayout>
  )
}

export default Registration
