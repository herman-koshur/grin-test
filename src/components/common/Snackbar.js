import React from "react"
import { useSelector, useDispatch } from "react-redux"

import MaterialSnackbar from "@mui/material/Snackbar"
import Alert from "@mui/material/Alert"

import { hideSnackbar } from "../../actions"

export const Snackbar = () => {
  const dispatch = useDispatch()
  const { open, text, duration, type } = useSelector(state => state.snackbar)

  const handleHideSnackbar = () => {
    dispatch(hideSnackbar())
  }

  return (
    <MaterialSnackbar
      open={open}
      autoHideDuration={duration}
      onClose={handleHideSnackbar}
    >
      <Alert severity={type} onClose={handleHideSnackbar}>
        {text}
      </Alert>
    </MaterialSnackbar>
  )
}
