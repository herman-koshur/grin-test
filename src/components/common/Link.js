import styled from "@emotion/styled"

export const Link = styled.a`
  text-decoration: none;
  color: inherit;
`
