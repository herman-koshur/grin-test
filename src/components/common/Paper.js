import styled from "@emotion/styled"

import MaterialPaper from "@mui/material/Paper"

export const Paper = styled(MaterialPaper)`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 20px;
  margin: 20px;
  width: 400px;
  max-width: 100%;
`
