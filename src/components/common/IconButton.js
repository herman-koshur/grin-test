import React from "react"
import PropTypes from "prop-types"

import Tooltip from "@mui/material/Tooltip"
import MaterialIconButton from "@mui/material/IconButton"

export const IconButton = ({
  children,
  onClick,
  tooltip,
  color = "primary",
}) => {
  return (
    <Tooltip title={tooltip}>
      <MaterialIconButton onClick={onClick} color={color}>
        {children}
      </MaterialIconButton>
    </Tooltip>
  )
}

IconButton.propTypes = {
  tooltip: PropTypes.string,
  color: PropTypes.string,
  children: PropTypes.node.isRequired,
  onClick: PropTypes.func.isRequired,
}
