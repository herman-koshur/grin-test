import React from "react"
import styled from "@emotion/styled"
import PropTypes from "prop-types"

import MaterialButton from "@mui/material/Button"
import MaterialCircularProgress from "@mui/material/CircularProgress"

const StyledButton = styled(MaterialButton)`
  position: relative;
`

const CircularProgress = styled(MaterialCircularProgress)`
  position: absolute;
`

export const Button = ({
  type = "button",
  variant = "contained",
  color = "primary",
  loading = false,
  disabled = false,
  children,
  onClick,
}) => {
  return (
    <StyledButton
      type={type}
      variant={variant}
      color={color}
      disabled={disabled}
      onClick={onClick}
    >
      {children}
      {loading && <CircularProgress size={20} />}
    </StyledButton>
  )
}

Button.propTypes = {
  type: PropTypes.string,
  variant: PropTypes.string,
  color: PropTypes.string,
  loading: PropTypes.bool,
  disabled: PropTypes.bool,
  children: PropTypes.node.isRequired,
  onClick: PropTypes.func,
}
