import React from "react"
import styled from "@emotion/styled"
import PropTypes from "prop-types"

import MaterialAvatar from "@mui/material/Avatar"
import Skeleton from "@mui/material/Skeleton"

const StyledAvatar = styled(MaterialAvatar)`
  cursor: pointer;
`

export const Avatar = ({ loading = false, src, size, alt, onClick }) => {
  return loading ? (
    <Skeleton variant="circular" width={size.width} height={size.height} />
  ) : (
    <StyledAvatar src={src} sx={size} alt={alt} onClick={onClick} />
  )
}

Avatar.propTypes = {
  loading: PropTypes.bool,
  src: PropTypes.string,
  size: PropTypes.object.isRequired,
  alt: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
}
