import React from "react"
import styled from "@emotion/styled"
import PropTypes from "prop-types"

import MaterialTypography from "@mui/material/Typography"
import Skeleton from "@mui/material/Skeleton"

const StyledTypography = styled(MaterialTypography)`
  ${({ value }) =>
    value && {
      cursor: "pointer",
      textDecoration: "underline",
    }}
`

export const Typography = ({
  variant,
  color = "inherit",
  children,
  loading = false,
  loadingWidth = 100,
  name = "",
  value = "",
  onClick,
}) => {
  const showAlternativeText = name && !value
  const textColor = showAlternativeText ? "primary" : color
  const text = showAlternativeText ? `Add ${name}` : children
  return (
    <StyledTypography
      variant={variant}
      color={textColor}
      value={showAlternativeText && !loading}
      onClick={showAlternativeText ? onClick : undefined}
    >
      {loading ? <Skeleton width={loadingWidth} /> : text}
    </StyledTypography>
  )
}

Typography.propTypes = {
  variant: PropTypes.string.isRequired,
  color: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.string,
  loading: PropTypes.bool,
  loadingWidth: PropTypes.number,
  children: PropTypes.node,
  onClick: PropTypes.func,
}
