import styled from "@emotion/styled"

export const Form = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  text-align: center;
  max-width: 400px;
  width: 100%;
  margin: 10px;
  box-sizing: border-box;

  ${({ maxWidth }) => maxWidth && { maxWidth }}
  ${({ maxWidth }) => maxWidth === "none" && { width: "auto" }}
  ${({ margin }) => margin && { margin }}

  div {
    margin-bottom: 10px;
  }

  .MuiTextField-root {
    width: calc(100% - 20px);
  }
`
