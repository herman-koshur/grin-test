import styled from "@emotion/styled"

export const ButtonGroup = styled.div`
  display: flex;
  justify-content: space-around;
  width: 100%;
  margin: 20px 0;
`
