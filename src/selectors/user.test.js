import { getUserData } from "."

const mockedState = {
  user: {
    data: {
      createdAt: "2021-09-23T00:00:00.000Z",
      bio: "Hey there!",
      handle: "John",
      email: "john.doe@test.com",
      location: "Los Angeles, US",
      website: "test.com",
      imageUrl: "test.com/image.png",
    },
  },
}

test("Get User Data selector", () => {
  const { bio, handle, email, location, website, imageUrl } =
    mockedState.user.data
  expect(getUserData(mockedState)).toEqual({
    createdAt: "Joined 23.09.2021",
    nickname: handle,
    bio,
    email,
    location,
    imageUrl,
    website,
  })
})
