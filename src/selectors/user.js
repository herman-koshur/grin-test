import { format } from "date-fns"

export const getUserData = state => {
  const userData = state.user.data
  return {
    createdAt: format(new Date(userData.createdAt || 0), "'Joined 'dd.MM.yyyy"),
    bio: userData.bio,
    nickname: userData.handle,
    location: userData.location,
    imageUrl: userData.imageUrl,
    website: userData.website,
    email: userData.email,
  }
}
