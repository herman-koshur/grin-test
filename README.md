# Grin Test App

This app was created for hard skill demo based on **`React`**.
It contains three pages: Login, Registration, User Profile where user is able to make auth and user actions.

In this app user is able to make next ajax requests using api url as prefix:

```js
post("/login") // sign in using email and password
post("/signup") // sign up using email, password, confirm password value and nickname
get("/user") // get data of authenticated user using token
post("/user") // set new data of authenticated user using token
post("/user/image") // set new avatar of authenticated user using token
```

## Available Scripts

In the app directory, you can run:

### `npm run start`

Runs the test app.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `npm run test`

Run all tests using **Jest**.

### `npm run build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.
Your app is ready to be deployed!

### `npm run lint`

Run **ESLint** with ignoring files from `.gitignore`.

### `npm run lint:fix`

Run **ESLint** with ignoring files from `.gitignore` and automatically fixing problems.

### `npm run prettify`

Run **Prettier** with ignoring files from `.prettierignore` and rewriting all processed files in place.
